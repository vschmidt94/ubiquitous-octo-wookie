#!/usr/bin/env python

# Name: Vaughan Schmidt
# ONID: schmicar@onid.oregonstate.edu
# Course: CS496-400, Spring 2015

# Credit to:
# CS496 Video lectures
# and tutorial located at: http://cloud.google.com/appengine/docs/python/gettingstartedpython27/helloworld

import webapp2

app = webapp2.WSGIApplication([
  ('/order', 'order.Order'),
  ('/edit', 'edit.Edit'),
  ('/', 'order.Order'),
  ], debug=True)