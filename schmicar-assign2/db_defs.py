from google.appengine.ext import ndb

class PizzaOrder(ndb.Model):
    # ndb PizzaOrder Model
    customer = ndb.StringProperty(required=True)
    password = ndb.StringProperty(required=True)
    size = ndb.StringProperty(required=True)
    quantity = ndb.StringProperty(required=True)
    toppings = ndb.StringProperty(repeated=True)
    comments = ndb.StringProperty(required=False)
    date = ndb.DateTimeProperty(auto_now_add=True)