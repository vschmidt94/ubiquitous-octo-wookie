# Name: Vaughan Schmidt
# ONID: schmicar@onid.oregonstate.edu
# Course: CS496-400, Spring 2015

# Credit to tutorial located at: 
# 1) CS496 tutorial videos 
# 2) http://cloud.google.com/appengine/docs/python/gettingstartedpython27/helloworld

# Imports
import webapp2
import base_page
from google.appengine.ext import ndb
import db_defs

class Order(base_page.BaseHandler):

  #over-ride initialization
  def __init__(self, request, response):
    self.initialize(request, response)
    self.template_values = {}

  def render(self, page):
    self.template_values['orders'] = [{
    'customer':x.customer, 
    'key':x.key.urlsafe(),
    'size':x.size,
    'quantity':x.quantity,
    'comments':x.comments,
    'toppings':x.toppings} for x in db_defs.PizzaOrder.query(ancestor=ndb.Key(db_defs.PizzaOrder,'base-order')).fetch()]
    base_page.BaseHandler.render(self, page, self.template_values)
  
  # what to do in case of http get request
  def get(self):
    self.render('order.html')

  # what to do in case of http post request
  def post(self):

    # get the action value from hidden form field
    action = self.request.get('action') 

    if action == 'new_order':
      k = ndb.Key(db_defs.PizzaOrder, 'base-order')
      myOrder = db_defs.PizzaOrder(parent = k)
      myOrder.customer = self.request.get('customer_name')  
      myOrder.password = self.request.get('customer_pw')
      myOrder.size = self.request.get('pizza_size')
      myOrder.quantity = self.request.get('qty')
      myOrder.toppings = [topping for topping in self.request.get_all('toppings')]
      myOrder.comments = self.request.get('spec_instr')
      myOrder.put()

    if action == 'update_order':
      order_key = ndb.Key(urlsafe=self.request.get('key'))
      myOrder = order_key.get()
      myOrder.customer = self.request.get('customer_name')  
      myOrder.password = self.request.get('customer_pw')
      myOrder.size = self.request.get('pizza_size')
      myOrder.quantity = self.request.get('qty')
      myOrder.toppings = [topping for topping in self.request.get_all('toppings')]
      myOrder.comments = self.request.get('spec_instr')
      myOrder.put()
    
    self.render('order.html')
    
