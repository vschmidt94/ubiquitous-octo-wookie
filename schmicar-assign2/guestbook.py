# Name: Vaughan Schmidt
# ONID: schmicar@onid.oregonstate.edu
# Course: CS496-400, Spring 2015

# Credit to tutorial located at: 
# http://cloud.google.com/appengine/docs/python/gettingstartedpython27/helloworld

# Imports
from google.appengine.api import users
from google.appengine.ext import ndb
import os
import urllib
import jinja2
import webapp2

# Jinja Templating
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

DEFAULT_ORDER_NAME = 'my_pizza_order'

def order_key(order_name=DEFAULT_ORDER_NAME):
    # ndb Datastore key for a Order entity.
    return ndb.Key('Order', order_name)


class Customer(ndb.Model):
    # ndb Customer model
    identity = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)


class PizzaOrder(ndb.Model):
    # ndb PizzaOrder Model
    customer = ndb.StructuredProperty(Customer)
    order_comments = ndb.StringProperty(indexed=False)
    date = ndb.DateTimeProperty(auto_now_add=True)


class MainPage(webapp2.RequestHandler):

    def get(self):
        order_name = self.request.get('order_name',
                                          DEFAULT_ORDER_NAME)
        my_pizza_orders_query = PizzaOrder.query(
            ancestor=order_key(order_name)).order(-PizzaOrder.date)
        my_pizza_orders = my_pizza_orders_query.fetch(10)

        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'my_pizza_orders': my_pizza_orders,
            'order_name': urllib.quote_plus(order_name),
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))



class Order(webapp2.RequestHandler):

    def post(self):
        # NOTE per Google tutorial: "the write
        # rate to a single entity group should be limited to ~1/second."
        # note to self: need to understand why
        order_name = self.request.get('order_name',
                                          DEFAULT_ORDER_NAME)
        my_pizza_order = PizzaOrder(parent=order_key(order_name))

        if users.get_current_user():
            my_pizza_order.customer = Customer(
                    identity=users.get_current_user().user_id(),
                    email=users.get_current_user().email())

        my_pizza_order.order_comments = self.request.get('order_comments')
        my_pizza_order.put()

        query_params = {'order_name': order_name}
        self.redirect('/?' + urllib.urlencode(query_params))


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/sign', Order),
], debug=True)