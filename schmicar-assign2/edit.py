# Name: Vaughan Schmidt
# ONID: schmicar@onid.oregonstate.edu
# Course: CS496-400, Spring 2015

# Credit to tutorial located at: 
# 1) CS496 tutorial videos 
# 2) http://cloud.google.com/appengine/docs/python/gettingstartedpython27/helloworld

# Imports
import webapp2
import base_page
from google.appengine.ext import ndb
import db_defs

class Edit(base_page.BaseHandler):

  #over-ride initialization
  def __init__(self, request, response):
    self.initialize(request, response)
    self.template_values = {}

  def get(self):
    if self.request.get('type') == 'order':
      order_key = ndb.Key(urlsafe=self.request.get('key'))
      order = order_key.get()
      self.template_values['key'] = self.request.get('key')
      self.template_values['customer'] = order.customer
      self.template_values['quantity'] = order.quantity
      self.template_values['comments'] = order.comments
      self.template_values['password'] = order.password
      self.template_values['toppings'] = order.toppings

    self.render('edit.html', self.template_values)
    
