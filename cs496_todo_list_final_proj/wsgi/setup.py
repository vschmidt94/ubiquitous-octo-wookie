from setuptools import setup

setup(name='assign32ws',
      version='1.0',
      description='CS419 Assignment 32',
      author='Vaughan Schmidt',
      author_email='schmicar@onid.oregonstate.edu',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask>=0.7.2', 'MarkupSafe' , 'Flask-SQLAlchemy>=0.16',],
     )
