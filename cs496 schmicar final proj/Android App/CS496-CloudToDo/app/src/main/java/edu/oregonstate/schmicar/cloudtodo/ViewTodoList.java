package edu.oregonstate.schmicar.cloudtodo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class ViewTodoList extends Activity {

    public static final String MY_PREFS = "MyPrefsFile";

    EditText todo_item_tag_field;
    EditText todo_item_desc_field;
    EditText todo_item_phone_field;

    public static final String TAG_ITEM_TAG = "itemtag";
    public static final String TAG_ITEM_DESC = "itemdesc";
    public static final String TAG_ITEM_PHONE = "itemphone";
    public static final String TAG_TODO = "todos";
    public static final String TAG_ITEMS = "items";

    public static final String theURL = "http://assign32ws-schmicar.rhcloud.com/ws/get_todos/";
    ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_todo_list);

        // Test for network connection
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected() ) {
            // we have network availability

            // get the JSON from Database
            new GetWebServiceData().execute();
        } else {
            // notify user of no network connection
            Context context = getApplicationContext();
            CharSequence msg = "No internet connection available, can not load data";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();
        }

        // Populate List View
        populateListView();

        // Register list click callback
        registerListClickCallback();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_todo_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Subclass GetWebServiceData uses HTTP Async Task to run on background thread.
     */
    private class GetWebServiceData extends AsyncTask<Void, Void, Void> {
        // help from http://www.androidhive.info/2012/01/android-json-parsing-tutorial/
        // and developer.android.com/training/basics/network-ops/connecting.html

        // Progress bar dialog
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // notify user the sync is starting
            Context context = getApplicationContext();
            CharSequence msg = "Accessing webservice...";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();
        } /* onPreExecute */

        @Override
        protected Void doInBackground(Void... arg0) {

            try {
                // Get the information
                downloadUrl(theURL);
            } catch (IOException e) {
                // notify user of no network connection
                Context context = getApplicationContext();
                CharSequence msg = "Error: Unable to download from webservice";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, msg, duration);
                toast.show();
            }

            return null;
        } /* End doInBackground */

        //onPostExecute dismisses the progress bar
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            // notify user the sync is done
            Context context = getApplicationContext();
            CharSequence msg = "Sync Complete";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();
            populateListView();
        } /* onPostExecute */

        /** Given a URL, establishes a HttpUrlConnection and retrieves the content as an InputStream,
         * which it parses into the HashMap structure used to fill the ListView.
         */
        private void downloadUrl(String myUrl) throws IOException {
            InputStream is = null;
            String contentString = null;

            // retrieve from shared preferences
            SharedPreferences prefs = getSharedPreferences(MY_PREFS, MODE_PRIVATE);
            final String theUser = prefs.getString("username", null);
            final String theToken = prefs.getString("usertoken", null);

            // finish up building the url
            myUrl += theUser;
            myUrl += "/";
            myUrl += theToken;

            Log.d("myUrl", myUrl);

            URL url = new URL(myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            try {
                // Set Timeouts
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Start the query
                conn.connect();
                int response = conn.getResponseCode();
                if (response == 200) // if got actual data
                {
                    is = conn.getInputStream();
                    // Convert the InputStream into a string
                    contentString = InputStreamToString(is);
                }
                // Makes sure InputStream's closed & connection's disconnected after app's finished using it
            } finally {
                if (is != null) {
                    is.close();
                }
                conn.disconnect();
            }

            // Take the string that was returned and parse out component tables to make the HashMaps
            if (contentString != null) {
                // Parse the JSON
                try {
                    JSONObject webserviceJSON = new JSONObject(contentString);

                    Log.d("SyncDB", "webserviceJSON = " + webserviceJSON.toString().substring(0,30));
                    JSONArray allTodosJSON = webserviceJSON.getJSONArray(TAG_TODO);

                    // break out items as their own JSONObject
                    JSONObject allItemsJSON = allTodosJSON.getJSONObject(0);
                    Log.d("allItemsJSON", allItemsJSON.toString().substring(0,40));

                    // and then into item array
                    JSONArray items = allItemsJSON.getJSONArray("items");
                    Log.d("items", items.toString().substring(0,40));


                    // Loop the array (credit: http://mobile.dzone.com/news/android-tutorial-how-parse)
                    for(int i=0; i < items.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject e = items.getJSONObject(i);
                        map.put(TAG_ITEM_TAG, e.getString(TAG_ITEM_TAG));
                        map.put(TAG_ITEM_DESC, e.getString(TAG_ITEM_DESC));
                        map.put(TAG_ITEM_PHONE, e.getString(TAG_ITEM_PHONE));
                        mylist.add(map);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } /* end DownloadURL() */

    } /* End GetWebService Data */

    public static String InputStreamToString(InputStream is1)  {
        BufferedReader rd = new BufferedReader(new InputStreamReader(is1), 4096);
        String line;
        StringBuilder sb =  new StringBuilder();
        try {
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        String contentOfMyInputStream = sb.toString();
        return contentOfMyInputStream;
    } /* End InputStreamToString */

    public void populateListView() {


        ListAdapter adapter = new SimpleAdapter(getApplicationContext(), mylist, R.layout.todos_layout,
                new String[] {TAG_ITEM_TAG,
                        TAG_ITEM_DESC,
                        TAG_ITEM_PHONE},
                new int[] {R.id.todo_item_tag,
                        R.id.todo_item_descr,
                        R.id.todo_item_phone} );

        // set the adapter for the list view
        ListView myList = (ListView)findViewById(R.id.listViewToDos);
        myList.setAdapter(adapter);
    }

    private void registerListClickCallback() {
        ListView myList = (ListView) findViewById(R.id.listViewToDos);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long idInDB) {
                Intent todoDetails = new Intent(ViewTodoList.this, ViewToDo.class);

                TextView tv_tag = (TextView)findViewById(R.id.todo_item_tag);
                TextView tv_desc = (TextView)findViewById(R.id.todo_item_descr);
                TextView tv_phone = (TextView)findViewById(R.id.todo_item_phone);
                String str_tag = tv_tag.getText().toString();
                String str_desc = tv_desc.getText().toString();
                String str_phone = tv_phone.getText().toString();

                todoDetails.putExtra("tag", str_tag);
                todoDetails.putExtra("desc", str_desc);
                todoDetails.putExtra("phone", str_phone);
                startActivity(todoDetails);
            }
        });
    }
}
