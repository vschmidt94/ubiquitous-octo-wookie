package edu.oregonstate.schmicar.cloudtodo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    public static final String MY_PREFS = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        /* Credit to classmate Lisa Percival for pointing out alternative persistent storage using
        shared preferences.... I had originally written using SQLite, but this is much more elegent
        and better suited for purpose. */
        SharedPreferences prefs = getSharedPreferences(MY_PREFS, MODE_PRIVATE);
        String restored_userName = prefs.getString("username", null);
        String restored_userToken = prefs.getString("usertoken", null);

        if(restored_userName != null) {
            ((MyApplication)this.getApplication()).setUserName(restored_userName);
        } else {
            ((MyApplication)this.getApplication()).setUserName(null);
        }

        if(restored_userToken != null) {
            ((MyApplication)this.getApplication()).setUserToken(restored_userToken);
        } else {
            ((MyApplication)this.getApplication()).setUserToken(null);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createNewAccount(View view) {
        // go to the create new user account activity
        Intent intent = new Intent(this, CreateNewAccount.class);
        startActivity(intent);
    }

    public void loginAccount(View view) {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void logoutAccount(View view) {
        // resets any shared preferences
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS, MODE_PRIVATE).edit();
        editor.putString("username", " ");
        editor.putString("usertoken", " ");
        editor.commit();

        // notify user the flush is done
        Context context = getApplicationContext();
        CharSequence msg = "All local user data has been flushed.";
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }

    public void viewCredentials(View view) {
        Intent intent = new Intent(this, ViewCredentials.class);
        startActivity(intent);
    }

    public void addTodoItem(View view) {
        Intent intent = new Intent(this, AddTodoItem.class);
        startActivity(intent);
    }

    public void viewMyList(View view) {
        Intent intent = new Intent(this, ViewTodoList.class);
        startActivity(intent);
    }
}
