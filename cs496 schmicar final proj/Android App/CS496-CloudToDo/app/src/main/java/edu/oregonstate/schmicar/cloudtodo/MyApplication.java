package edu.oregonstate.schmicar.cloudtodo;

import android.app.Application;
import android.content.SharedPreferences;

/**
 * Created by carlschmidt on 6/1/15.
 *
 * This class extends the main application class to allow for some global variables with
 * persistence across all activities
 *
 * Credit: http://stackoverflow.com/questions/1944656/android-global-variable
 */
public class MyApplication extends Application {


    public static final String MY_PREFS = "MyPrefsFile";
    private String userName;
    private String userToken;

    public String getUserName() {
        return userName;
    }

    public String getUserToken() {
        return userToken;
    }

//    public String getUserPassword() {
//        return userPassword;
//    }

    public void setUserName(String name) {
        this.userName = name;
    }

    public void setUserToken(String token) {
        this.userToken = token;
    }

//    public void setUserPassword(String password) {
//        this.userPassword = password;
//    }
}
