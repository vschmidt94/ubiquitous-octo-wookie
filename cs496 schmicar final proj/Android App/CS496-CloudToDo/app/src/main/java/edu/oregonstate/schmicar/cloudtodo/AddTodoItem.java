package edu.oregonstate.schmicar.cloudtodo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class AddTodoItem extends Activity {

    public static final String MY_PREFS = "MyPrefsFile";

    EditText todo_item_tag_field;
    EditText todo_item_desc_field;
    EditText todo_item_phone_field;
    String todo_item;
    String todo_tag;
    String todo_phone;

    public static final String TAG_ITEM_TAG = "itemtag";
    public static final String TAG_ITEM_DESC = "itemdesc";
    public static final String TAG_ITEM_PHONE = "itemphone";
    public static final String TAG_USER_ID = "userid";
    public static final String TAG_USER_TOKEN = "usertoken";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo_item);

        todo_item_desc_field = (EditText) findViewById(R.id.textView14);
        todo_item_tag_field = (EditText) findViewById(R.id.todo_item_tag);
        todo_item_phone_field = (EditText) findViewById(R.id.todo_item_phone);

        // retrieve from shared preferences
        SharedPreferences prefs = getSharedPreferences(MY_PREFS, MODE_PRIVATE);
        final String theUser = prefs.getString("username", null);
        final String theToken = prefs.getString("usertoken", null);

        Button add_todo_button = (Button) findViewById(R.id.todo_add_item_button);
        add_todo_button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View V) {
                // get data from form
                todo_tag = todo_item_tag_field.getText().toString();
                todo_item = todo_item_desc_field.getText().toString();
                todo_phone = todo_item_phone_field.getText().toString();

                // add data to name-value pairs for POST form
                final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair(TAG_USER_ID, theUser));
                nameValuePairs.add(new BasicNameValuePair(TAG_USER_TOKEN, theToken));
                nameValuePairs.add(new BasicNameValuePair(TAG_ITEM_DESC, todo_item));
                nameValuePairs.add(new BasicNameValuePair(TAG_ITEM_TAG, todo_tag));
                nameValuePairs.add(new BasicNameValuePair(TAG_ITEM_PHONE, todo_phone));

                // execute Async Task to post
                new MyAsyncTask().execute(nameValuePairs);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_todo_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* MyAsyncTask class is what will post form data to web service asynchronously */
    private class MyAsyncTask extends AsyncTask<List<NameValuePair>, Void, Void> {

        String response_string;

        protected void onPostExecute(Void result) {

            // notify user the flush is done
            Context context = getApplicationContext();
            CharSequence msg = "Server Response: " + response_string;
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();

            // clear the text fields
            todo_item_desc_field = (EditText) findViewById(R.id.textView14);
            todo_item_tag_field = (EditText) findViewById(R.id.todo_item_tag);
            todo_item_phone_field = (EditText) findViewById(R.id.todo_item_phone);

            todo_item_desc_field.setText("");
            todo_item_tag_field.setText("");
            todo_item_phone_field.setText("");

        }

        protected Void doInBackground(List<NameValuePair>... nameValuePairs) {
            // get zero index of nameValuePairs
            List<NameValuePair> nvPairs = nameValuePairs[0];
            Log.i("nvPairs[0]", nameValuePairs[0].toString());

            InputStream inputStream = null;

            try {
                // Create a new HttpClient and Post Header
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost myhttppost = new HttpPost("http://assign32ws-schmicar.rhcloud.com/ws/additem");

                // create http post form entity
                myhttppost.setEntity(new UrlEncodedFormEntity(nvPairs));

                // make HTTP post request
                HttpResponse response = httpclient.execute(myhttppost);

                // receive response as input stream
                inputStream = response.getEntity().getContent();

                // handle the response
                //content.setText(response.toString());
                response_string = convertInputStreamToString(inputStream);
                Log.i("addItem", response_string);

                // parse the reply for token
                if(response_string != null) {
                    // parse the JSON
                    try {
                        JSONObject webserviceJSON = new JSONObject(response_string);
                        Log.i("NewItem", "JSON= " + webserviceJSON);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (ClientProtocolException e) {
                // Log exception
                e.printStackTrace();
                //content.setText(e.toString());
            } catch (IOException e) {
                // Log exception
                e.printStackTrace();
                //content.setText(e.toString());
            }

            return null;

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        // Credit: http://hmkcode.com/android-internet-connection-using-http-get-httpclient/
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
