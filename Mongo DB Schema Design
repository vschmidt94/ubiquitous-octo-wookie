Mongo DB Schema Design

Example:  Books in booksstore

book = {author: "Herge",
        date: new Date(),
        text: "Destination Moon",
        tags: ["comic", "adventure"]}

> db.books.save(book)     <-- books collection created automagically
 
 every book can have own set of fields

 tags field is array of strings

 Once book is inserted:

  > db.books.find()     <-- like select * from in sql

 Secondary index on Author:
  > db.books.ensureIndex({author: 1})
  > db.books.find({author: 'Herge'})

  *dont have to have index, but it speeds up a lot

Examine the query plan:

  > db.books.find({author: 'Herge'}).explain()

Multi-key indexes:
  // build index on tags array:
  > db.books.ensureIndex({tags: 1})
  > db.books.find({tags: 'comic'})

Query Operators
  $ne, $in, $nin, $mod, $all, $size, $exists, $type,
  $lt, $gt, etc

Extending the schema:
  new_comment = { author: "Kyle",
  date: new_Date(),
  text: "great book",
  votes: 5 }

  > db.books.update(
        {text: "Destination Moon"},
        { '$push': {comments: new_comment},    <-- adds to the doc pushes value onto array
          '$inc': {comments_counts: 1}})       <-- adds if not there, and increments

The dot operator:

  // create index on nested documents:
  > db.books.endureIndex({"comments.author": 1})
  > db.books.find({comments.author: "Kyle"})

  // create an index to comment votes:
  > db.books.ensureIndex({comments.votes: 1})
  // find all booxs with any comments with more than 50 votes
  > db.books.ensureIndex({comments.votes: {$gt: 50}})

  // create a sparse index
  > db.shapes.ensureIndex({radius: 1}, {sparse: true})
  ^^^ sparse index is good for cases where not all docs may have field.
  ^^^ if you did regular index w/o sparse, the index field would be added to all
  ^^  documents, but with null value where it did not already exist

