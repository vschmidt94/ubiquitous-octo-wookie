package edu.oregonstate.schmicar.androidwebpost;

// Credit to:
// http://hayageek.com/android-http-post-get/
// http://www.vogella.com/tutorials/ApacheHttpClient/article.html
// http://developer.android.com/reference/org/apache/http/client/methods/HttpPost.html
// http://androidexample.com/How_To_Make_HTTP_POST_Request_To_Server_-_Android_Example/index.php?view=article_discription&aid=64&aaid=89

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.HttpResponse;


public class MainActivity extends Activity {

    TextView content;
    EditText co_name, co_street, co_city, co_state, co_zip;
    String Name, Street, City, State, Zip;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        content = (TextView) findViewById(R.id.content);
        co_name = (EditText) findViewById(R.id.editCoName);
        co_street = (EditText) findViewById(R.id.editCoStreet);
        co_city = (EditText) findViewById((R.id.editCoCity));
        co_state = (EditText) findViewById(R.id.editCoState);
        co_zip = (EditText) findViewById(R.id.editCoZip);

        Button postme = (Button) findViewById(R.id.postData);
        postme.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                // get data from form
                Name = co_name.getText().toString();
                Street = co_street.getText().toString();
                City = co_city.getText().toString();
                State = co_state.getText().toString();
                Zip = co_zip.getText().toString();

                // add data to name-value pairs for post form
                final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name", Name));
                nameValuePairs.add(new BasicNameValuePair("street", Street));
                nameValuePairs.add(new BasicNameValuePair("city", City));
                nameValuePairs.add(new BasicNameValuePair("state", State));
                nameValuePairs.add(new BasicNameValuePair("zip", Zip));

                new MyAsyncTask().execute(nameValuePairs);
            }
        });

        Button getme = (Button) findViewById(R.id.getData);
        getme.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                // Call async task to get last record from webservice database
                new HttpAsyncTask().execute("http://www.corvallisrecycles.org/api/testing/lastComp.php");

            }
        });

    }

    public void locateMe(View view) {
        // called when user hits Locate Me button
        Intent intent = new Intent(this, LocateMeActivity.class);
        startActivity(intent);
    }


    private class MyAsyncTask extends AsyncTask<List<NameValuePair>, Void, Void> {

        protected void onPostExecute(){
            content.setText("Done");
        }

        protected Void doInBackground(List<NameValuePair>... nameValuePairs) {
            // get zero index of nameValuePairs
            List<NameValuePair> nvPairs = nameValuePairs[0];
            Log.i("nvPairs[0]", nameValuePairs[0].toString());

            try {
                // Create a new HttpClient and Post Header
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost myhttppost = new HttpPost("http://www.corvallisrecycles.org/api/testing/addComp.php");

                // create http post form entity
                myhttppost.setEntity(new UrlEncodedFormEntity(nvPairs));

                // make HTTP post request
                HttpResponse response = httpclient.execute(myhttppost);

                // handle the response
                //content.setText(response.toString());
                Log.i("postData", response.getStatusLine().toString());

            } catch (ClientProtocolException e) {
                // Log exception
                e.printStackTrace();
                //content.setText(e.toString());
            } catch (IOException e) {
                // Log exception
                e.printStackTrace();
                //content.setText(e.toString());
            }

            return null;

        }
    }

    public static String GET(String url){
        // Credit: http://hmkcode.com/android-internet-connection-using-http-get-httpclient/
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        // Credit: http://hmkcode.com/android-internet-connection-using-http-get-httpclient/
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        // Credit: http://hmkcode.com/android-internet-connection-using-http-get-httpclient/
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();
            content.setText(result);
        }
    }


}

