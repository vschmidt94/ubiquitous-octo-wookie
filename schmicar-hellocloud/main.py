#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import time
import datetime
import urllib2
import json


class MainHandler(webapp2.RequestHandler):
    def get(self):
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        f = urllib2.urlopen('http://api.wunderground.com/api/f7ec8190c3e1c837/geolookup/conditions/q/OR/Salem.json')
        weather_string = f.read()
        parsed_weather = json.loads(weather_string)
        location = parsed_weather['location']['city']
        temp_f = parsed_weather['current_observation']['temp_f']
        obs_time = parsed_weather['current_observation']['observation_time_rfc822']
        self.response.write('<h3>CS496 Hello Cloud Assignment<br><br>Name: Vaughan Schmidt <br>ONID: schmicar</h3>')
        self.response.write('The current SYSTEM time is: ' + st + ' (ZULU / GMT)<br>')
        self.response.write('The last weather observation time was: %s <br>' % obs_time)
        self.response.write('The current temperature in %s is: %s' % (location, temp_f))
        #self.response.write(parsed_weather)
        self.response.write('<span style="display: block !important; width: 180px; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://www.wunderground.com/cgi-bin/findweather/getForecast?query=zmw:97301.1.99999&bannertypeclick=wu_travel_jet1" title="Salem, Oregon Weather Forecast" target="_blank"><img src="http://weathersticker.wunderground.com/weathersticker/cgi-bin/banner/ban/wxBanner?bannertype=wu_travel_jet1&airportcode=KSLE&ForcedCity=Salem&ForcedState=OR&zip=97302&language=EN" alt="Find more about Weather in Salem, OR" width="160" /></a><br><a href="http://www.wunderground.com/cgi-bin/findweather/getForecast?query=zmw:97301.1.99999&bannertypeclick=wu_travel_jet1" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" target="_blank">Click for weather forecast</a></span>')

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
