package edu.oregonstate.schmicar.androidactionbar;
import android.util.Log;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.HashMap;
import java.util.ArrayList;
/**
 * Created by carlschmidt on 5/19/15.
 */
public class HashMapHelper {
    public static ArrayList<HashMap<String, String>> getCompaniesFromJSON(JSONObject original){
        ArrayList result = new ArrayList<HashMap<String, String>>();
        try {
            Log.i("OBJ", original.toString().substring(0,30));
            JSONObject db = original.getJSONObject("Database");
            Log.e("HELPER", "11111");
            JSONObject arro = db.getJSONObject("Companies");
            Log.e("HELPER", "11111222");
            JSONArray arr = db.getJSONArray("Companies");
            Log.e("HELPER", "111113333");
            Log.e("HELPER", "1");
            for (int i = 0; i < arr.length(); i++) {
                Log.e("HELPER", i+"1");
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("cmp_id", arr.getJSONObject(i).getString("cmp_id"));
                map.put("cmp_name", arr.getJSONObject(i).getString("cmp_name"));
                map.put("cmp_address1", arr.getJSONObject(i).getString("cmp_address1"));

                ///
                result.add(map);
            }
        }catch(Exception ex){
            //
            //ex.printStackTrace();
            Log.e("HELPER", ex.getMessage());
        }
        return result;
    }
}
