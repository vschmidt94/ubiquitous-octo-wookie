package edu.oregonstate.schmicar.androidactionbar;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new FetchJSONDataTask().execute("http://www.corvallisrecycles.org/api/interface.php?cmp=1&cat=1&itm=1");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // reads the URL
    public static JSONObject GET(String url){
        // Credit: http://hmkcode.com/android-internet-connection-using-http-get-httpclient/
        InputStream inputStream = null;
        String result = "";


        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null) {
                result = convertInputStreamToString(inputStream);
                //Log.i("String", result);
            }
            else
                result = "Did not work!";

            JSONObject obj = new JSONObject(result);

            Log.i("JSON", obj.getJSONObject("Database").getJSONObject("Categories").getString("cat_name"));

            return obj;

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return(null);

    }

    private class FetchJSONDataTask extends AsyncTask<String, Void, JSONObject> {

        // execute on background thread
        protected JSONObject doInBackground(String... urls) {
            return GET(urls[0]);
        }

        // gets executed in UI thread
        protected void onPostExecute(JSONObject json) {
            // take JSON object and populate database
            DBController myDBController = new DBController( getApplicationContext() );

            // get hashmap from JSON object
            ArrayList<HashMap<String, String>> companies = HashMapHelper.getCompaniesFromJSON(json);
            for(HashMap<String, String> map: companies){
                myDBController.insertCompany(map);
            }

            // get companies from SQLite D
            SQLiteDatabase database =myDBController.getWritableDatabase();
            ArrayList<HashMap<String, String>> c = myDBController.getAllCompanies(database);
            Log.i("DB ", c.size()+"" +companies.size());

        }


    }



    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        // Credit: http://hmkcode.com/android-internet-connection-using-http-get-httpclient/
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
