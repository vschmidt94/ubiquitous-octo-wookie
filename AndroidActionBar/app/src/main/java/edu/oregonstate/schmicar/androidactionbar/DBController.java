package edu.oregonstate.schmicar.androidactionbar;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBController  extends SQLiteOpenHelper {

    public DBController(Context applicationcontext) {
        super(applicationcontext, "user.db", null, 1);
    }
    //Creates Table
    @Override
    public void onCreate(SQLiteDatabase database) {
        String queryCmp, queryCat, queryItm, queryCmpCat, queryItmCat;
        queryCmp = "CREATE TABLE companies ( cmp_id INTEGER, " +
                "cmp_name TEXT, " +
                "cmp_address1 TEXT, " +
                "cmp_address2 TEXT, " +
                "cmp_city TEXT, " +
                "cmp_state TEXT, " +
                "cmp_zip TEXT, " +
                "cmp_phone TEXT, " +
                "cmp_email TEXT, " +
                "cmp_website TEXT, " +
                "cmp_notes TEXT, " +
                "cmp_recycle_flag INTEGER," +
                "cmp_repair_flag, INTEGER)";

        queryCat = "CREATE TABLE Categories ( cat_id INTEGER, " +
                "cat_name TEXT, " +
                "cat_descr TEXT, " +
                "cat_notes TEXT)";

        queryItm = "CREATE TABLE Items ( itm_id INTEGER, " +
                "itm_name TEXT, " +
                "itm_descr TEXT, " +
                "itm_notes TEXT)";

        queryCmpCat = "CREATE TABLE CompanyCategories ( cmp_id INTEGER, " +
                "cat_id INTEGER, " +
                "comp_cat_notes TEXT)";

        queryItmCat = "CREATE TABLE ItemCategories ( itm_id INTEGER, " +
                "cat_id INTEGER," +
                "item_cat_notes TEXT)";

        database.execSQL(queryCmp);
        database.execSQL(queryCat);
        database.execSQL(queryItm);
        database.execSQL(queryCmpCat);
        database.execSQL(queryItmCat);

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query;
        query = "DROP TABLE IF EXISTS companies; " +
                "DROP TABLE IF EXISTS categories; " +
                "DROP TABLE IF EXISTS items; " +
                "DROP TABLE IF EXISTS CompanyCategories; " +
                "DROP TABLE IF EXISTS ItemCategories";
        database.execSQL(query);
        onCreate(database);
    }

    /**
     * Inserts User into SQLite DB
     * @param queryValues
     */
    public void insertCompany(HashMap<String, String> queryValues) {
        // inserts a record into the database
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (String s: queryValues.keySet()){
            values.put(s, queryValues.get(s));
        }
        database.insert("companies", null, values);
        database.close();
    }

    /**
     * Get list of Companies from SQLite DB as Array ListmyDBController.get
     * @return
     */
    public ArrayList<HashMap<String,String>> getAllCompanies(SQLiteDatabase database) {
        ArrayList<HashMap<String, String>> companyList;
        companyList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM companies";

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("userId", cursor.getString(0));
                map.put("userName", cursor.getString(1));
                companyList.add(map);
            } while (cursor.moveToNext());
        }
        database.close();
        return companyList;
    }

}
