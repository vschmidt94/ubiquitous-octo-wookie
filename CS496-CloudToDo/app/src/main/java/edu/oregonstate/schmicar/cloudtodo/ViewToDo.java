package edu.oregonstate.schmicar.cloudtodo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;


public class ViewToDo extends ActionBarActivity {

    public static final String theURL = "http://assign32ws-schmicar.rhcloud.com/ws/del_todo/";
    public static final String MY_PREFS = "MyPrefsFile";
    String tag;
    String desc;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_to_do);

        // get extras from intent and populate view
        Intent myIntent = getIntent();
        tag = myIntent.getStringExtra("tag");
        desc = myIntent.getStringExtra("desc");
        phone = myIntent.getStringExtra("phone");

        TextView tv_tag = (TextView)findViewById(R.id.todo_detail_tag);
        TextView tv_desc = (TextView)findViewById(R.id.todo_detail_descr);
        TextView tv_phone = (TextView)findViewById(R.id.todo_detail_phone);

        tv_tag.setText(tag);
        tv_desc.setText(desc);
        tv_phone.setText(phone);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_to_do, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void dialNumber(View view) {
        TextView tv_phone = (TextView)findViewById(R.id.todo_detail_phone);
        String numberString = tv_phone.getText().toString();

        if(!numberString.equals("")) {
            Uri number = Uri.parse("tel:" + numberString);
            Intent dial = new Intent(Intent.ACTION_CALL, number);
            startActivity(dial);
        }

    }

    /**
     * private class to make async http DELETE call to web service
     */
   private class deleteTodo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // notify user delete is starting
            Context context = getApplicationContext();
            CharSequence msg = "Sending DELETE to webservice";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                // Get the information
                deleteRequestUrl(theURL);
            } catch (IOException e) {
                // notify user of no network connection
                Context context = getApplicationContext();
                CharSequence msg = "Error: Unable to download from webservice";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, msg, duration);
                toast.show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            // notify user the sync is done
            Context context = getApplicationContext();
            CharSequence msg = "To-Do has been deleted";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();
        } /* onPostExecute */

        /** Given a URL, establishes a HttpUrlConnection and retrieves the content as an InputStream,
         * which it parses into the HashMap structure used to fill the ListView.
         */
        private void deleteRequestUrl(String myUrl) throws IOException {

            // retrieve from shared preferences
            SharedPreferences prefs = getSharedPreferences(MY_PREFS, MODE_PRIVATE);
            final String theUser = prefs.getString("username", null);
            final String theToken = prefs.getString("usertoken", null);

            // finish up building the url
            myUrl += theUser;
            myUrl += "/";
            myUrl += theToken;
            myUrl += "/";
            myUrl += tag;
            myUrl += "/";
            myUrl += desc;

            Log.d("deleteTodo", "URL = " + myUrl);

            URL url = new URL(myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            try {
                // Set Timeouts
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("DELETE");
                conn.setDoInput(true);
                // Start the query
                conn.connect();
                int response = conn.getResponseCode();

            } finally {
                // close connection
                conn.disconnect();
            }



        } /* end DownloadURL() */
   }
}
